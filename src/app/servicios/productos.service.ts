import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';


export interface product{
  id: string
  tipo: string
  img: string
}


@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  constructor(private db : AngularFirestore) { }

  getProductos(){
    return this.db.collection('productos').snapshotChanges().pipe(map(rooms => {
      return rooms.map(a =>{
        const data = a.payload.doc.data() as product;
        data.id = a.payload.doc.id;
        return data

      })
    })) 
  }
}


