import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { promise } from 'protractor';
import { resolve } from 'url';
import { userInfo } from 'os';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(private AFauth : AngularFireAuth, private router : Router, private db : AngularFirestore) { }

  login(email:string, password:string){
    return new Promise((resolve, rejected) =>{
      this.AFauth.signInWithEmailAndPassword(email, password).then(user => {
        resolve(user);
      }).catch(err => rejected(err));
    });
  } 

  logout(){
    this.AFauth.signOut().then(() => {
      this.router.navigate(['/inicio']);
    });
  }

  register(email : string, password : string, name : string, edad : string, sexo : string){ 
    return new Promise((resolve, reject) => {
      this.AFauth.createUserWithEmailAndPassword(email, password).then( res =>{
        
        const uid = res.user.uid;
        this.db.collection('users').doc(uid).set({
          name : name,
          edad : edad,
          sexo : sexo,
          uid : uid

        })
        resolve(res)
      }).catch( err => reject(err))
    })
  }
}
