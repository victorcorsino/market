import { Component, OnInit } from '@angular/core';
import { AuthService } from '../servicios/auth.service';
import { ActionSheetController } from '@ionic/angular';
import { ProductosService, product } from '../servicios/productos.service';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{

  public productos: any = [];

  constructor(public authservice : AuthService, public actionSheetController: ActionSheetController, public productoService : ProductosService) {}
  
  ngOnInit() {
    this.productoService.getProductos().subscribe( prod => {
      this.productos = prod;
    })
  }



  Onlogout(){
    this.authservice.logout();
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Opciones',
      buttons: [{
        text: 'Desconectarse',
        role: 'destructive',
        icon: 'log-out-outline',
        handler: () => {
          this.Onlogout()
        }
      }]
    });
    await actionSheet.present();
  }

  openProducto(product){


  }

}
