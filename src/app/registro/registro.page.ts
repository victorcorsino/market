import { Component, OnInit } from '@angular/core';
import { AuthService } from '../servicios/auth.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {

  public email : string;
  public name : string;
  public password : string;
  public edad : string;
  public sexo : string;

  public picture: string;

  constructor(private auth : AuthService, private router : Router, private afAuth: AngularFireAuth ) { }

  ngOnInit() {
  }

  
  OnSubmitRegister(){
    this.auth.register(this.email, this.password, this.name, this.edad, this. sexo).then( auth => {
      this.router.navigate(['inicio'])
      console.log(auth)
    }).catch(err => console.log(err))
  }


  

}
